# JAHAS 

Just Another Health Application Service

##Synopsis##

A school project for Sensor Based Mobile Application course at Metropolia University of Applied Sciences

Frontend Android application created for groups. The application counts users' steps. The steps can be committed to backend for a certain group. The group's steps are summed and added to the group's step count. All members will see the group's walked distance on a wold map.

Backend for the application can be found here:
https://github.com/juhanivl/jahas-app-backend

##NOTES##
- The backend must be running on the same network than the Android device is in
- The IP address & the port of the server should be configured in resource file **server_and_sensor_configuration**
- The MAC address of the MetaWear external sensor must be configured in resource file **server_and_sensor_configuration** 
- For Huawei devices: In order to get the StepCounter running on background even when the screen is off, add Jahas application to protected apps in **Settings -> Advanced settings -> Battery manager -> Protected apps**
- When choosing a route for the group, **suomi_tutuksi** is currently the best choice as the step amounts are calculated properly and the city data is accurate

##Screenshots##
The application navigation consist of three main views. The views are navigated around with a Bottom Bar Navigation.



![accountview.png](https://bitbucket.org/repo/bMMMje/images/490124452-accountview.png)

The view for creating account and creating/changing group.



![mapview.png](https://bitbucket.org/repo/bMMMje/images/2655401932-mapview.png)

The view for seeing the progress of the group.



![stepcount.png](https://bitbucket.org/repo/bMMMje/images/1203473661-stepcount.png)

The view displaying the step count and current temperature if the MetaWear sensor is connected.