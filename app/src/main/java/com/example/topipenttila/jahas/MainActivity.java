package com.example.topipenttila.jahas;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.Formatter;
import android.util.Log;
import android.widget.Toast;

import com.mbientlab.metawear.MetaWearBleService;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.BottomBarFragment;
import com.roughike.bottombar.OnMenuTabSelectedListener;

import org.greenrobot.eventbus.EventBus;

import java.util.Calendar;

import static com.google.android.gms.analytics.internal.zzy.f;
import static com.google.android.gms.analytics.internal.zzy.s;

public class MainActivity extends AppCompatActivity {



    private CoordinatorLayout coordinatorLayout;

    private Intent mServiceIntent;
    private StepCounterService mSensorService;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private SharedPreferences.OnSharedPreferenceChangeListener listener;

    private BottomBar bottomBar;

    private Context ctx;

    public Context getCtx() {
        return ctx;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctx = this;
        setContentView(R.layout.activity_main);


        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        editor = sharedPreferences.edit();

        listener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                //Show BottomBar when groupId set
                if (key == "groupId") {
                    bottomBar.show();
                }
            }
        };
        sharedPreferences.registerOnSharedPreferenceChangeListener(listener);

        /**
         * IMPORTANT!
         * Set the ip address of the server in resources file called server_and_sensor_configuration.xml
         * this will be saved to the shared preferences and used in AsyncTasks over to the API.
         */
        Log.e("ip is: ", sharedPreferences.getString("ip", "no ip"));


        ConnectivityManager aConnectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo aNetworkInfo = aConnectivityManager.getActiveNetworkInfo();
        if (aNetworkInfo != null && aNetworkInfo.isConnected()){
            Toast.makeText(this, "Connected to Internet", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "Internet Connection Timeout", Toast.LENGTH_LONG).show();
        }



        mSensorService = new StepCounterService(getCtx());
        mServiceIntent = new Intent(getCtx(), mSensorService.getClass());
        if (!isMyServiceRunning(mSensorService.getClass())) {
            startService(mServiceIntent);
        }



        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.three_buttons_activity);

        bottomBar = BottomBar.attach(this, savedInstanceState);
        bottomBar.setItemsFromMenu(R.menu.three_buttons_menu, new OnMenuTabSelectedListener() {


            @Override
            public void onMenuItemSelected(int itemId) {

                FragmentTransaction ft = getFragmentManager().beginTransaction();

                switch (itemId) {
                    case R.id.account:
                        System.out.println("account item");
                        ft.replace(R.id.fragmentContainer, new AccountFragment());
                        break;
                    case R.id.map:
                        ft.replace(R.id.fragmentContainer, new MapViewFragment());
                        System.out.println("map item");
                        break;
                    case R.id.stepCount:
                        ft.replace(R.id.fragmentContainer, new StepCounterFragment());
                        System.out.println("step count");
                        break;
                }
                ft.commit();
            }
        });


        /*Set bottom bar default position to be AccountFragment*/
        FragmentTransaction ft = getFragmentManager().beginTransaction();

        ft.replace(R.id.fragmentContainer, new AccountFragment());
        ft.commit();

        // Set the color for the active tab. Ignored on mobile when there are more than three tabs.
        bottomBar.setActiveTabColor("#C2185B");
        // Use the dark theme. Ignored on mobile when there are more than three tabs.
        //bottomBar.useDarkTheme(true);

        // Use custom text appearance in tab titles.
        //bottomBar.setTextAppearance(R.style.MyTextAppearance);

        // Use custom typeface that's located at the "/src/main/assets" directory. If using with
        // custom text appearance, set the text appearance first.
        //bottomBar.setTypeFace("MyFont.ttf");

        if (sharedPreferences.getString("groupId", "").equals("")) {
            bottomBar.hide();
        }

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i ("isMyServiceRunning?", true+"");
                return true;
            }
        }
        Log.i ("isMyServiceRunning?", false+"");
        return false;
    }



    @Override
    protected void onDestroy() {
        stopService(mServiceIntent);
        Log.i("MAINACT", "onDestroy!");
        super.onDestroy();


    }

}



