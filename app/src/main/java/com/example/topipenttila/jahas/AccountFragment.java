package com.example.topipenttila.jahas;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import org.json.JSONArray;
import org.json.JSONObject;

import static android.view.View.GONE;
import static com.google.android.gms.analytics.internal.zzy.e;

/**
 * Created by Juhani on 6.10.2016.
 * AccountFragment for account view to handle signings to groups,
 * creating groups, changing account name.
 */

public class AccountFragment extends Fragment {
    private AlertDialog levelDialog;
    private JSONObject[] updatableJSONObject = new JSONObject[1];
    private String changingOrCreating = "";
    private boolean groupNameExists;


    private Spinner routeSpinner;
    private Button submitGroupChanges;

    private TextView groupEditStatus, groupErrorTextView;
    private EditText userName, groupName, ipAddressBar;

    private ImageButton editUserProperties, saveUserProperties, editGroupProperties, saveIpProperties, editIpProperties;

    private SharedPreferences sharedPreferences;
    private View rootView;

    private InputMethodManager imm;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.account_view, container, false);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        editUserProperties = (ImageButton) rootView.findViewById(R.id.editUserProperties);
        saveUserProperties = (ImageButton) rootView.findViewById(R.id.saveUserProperties);

        editIpProperties = (ImageButton) rootView.findViewById(R.id.editIpProperties);
        saveIpProperties = (ImageButton) rootView.findViewById(R.id.saveIpProperties);

        editGroupProperties = (ImageButton) rootView.findViewById(R.id.editGroupProperties);
        submitGroupChanges = (Button) rootView.findViewById(R.id.submitGroupChanges);

        routeSpinner = (Spinner) rootView.findViewById(R.id.routeSpinner);
        routeSpinner.setEnabled(false);
        submitGroupChanges.setEnabled(false);
        submitGroupChanges.setBackgroundColor(Color.LTGRAY);

        groupEditStatus = (TextView) rootView.findViewById(R.id.changingOrCreating);
        groupErrorTextView = (TextView) rootView.findViewById(R.id.groupErrorTextView);

        userName = (EditText) rootView.findViewById(R.id.userName);
        groupName = (EditText) rootView.findViewById(R.id.groupName);
        ipAddressBar = (EditText) rootView.findViewById(R.id.ipAddressBar);

        imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        /*addTextChangedListener listens to keyboard events when user types to groupName
         * Each time user stops typing call checkIfGroupExists*/
        groupName.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    String groupToCompare = String.valueOf(groupName.getText());

                    checkIfGroupExists(groupToCompare, changingOrCreating);
                }

            }
        });


        /*Check if user runs app for the first time*/
        if (sharedPreferences.getBoolean("firstRun", true)) {
            sharedPreferences.edit().putString("ip", getResources().getString(R.string.ip)).apply();
            sharedPreferences.edit().putBoolean("firstRun", false).apply();
            welcomeDialog();

        } else {

            Log.e("userName: ", sharedPreferences.getString("userName", "no name"));
            Log.e("groupName: ", sharedPreferences.getString("groupName", "no group"));
            Log.e("groupRoute: ", sharedPreferences.getString("groupRoute", "no route"));
            Log.e("groupId", sharedPreferences.getString("groupId", "no Id"));

            /*User has run application before, get values from shared preferences*/
            userName.setText(sharedPreferences.getString("userName", ""));
            groupName.setText(sharedPreferences.getString("groupName", ""));
            ipAddressBar.setText(sharedPreferences.getString("ip" , ""));

            ArrayAdapter routeSpinnerAdapter = (ArrayAdapter) routeSpinner.getAdapter(); //cast to an ArrayAdapter
            int spinnerPosition = routeSpinnerAdapter.getPosition(sharedPreferences.getString("groupRoute", "noRoute"));
            routeSpinner.setSelection(spinnerPosition);
        }

        editIpProperties.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveIpProperties.setVisibility(View.VISIBLE);
                ipAddressBar.setEnabled(true);
                editIpProperties.setVisibility(View.INVISIBLE);
                ipAddressBar.requestFocus();
                imm.showSoftInput(ipAddressBar, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        saveIpProperties.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editIpProperties.setVisibility(View.VISIBLE);
                ipAddressBar.setEnabled(false);
                saveIpProperties.setVisibility(View.INVISIBLE);
                sharedPreferences.edit().putString("ip", String.valueOf(ipAddressBar.getText())).apply();
                showToast("Server's IP Address saved.");
            }
        });

        editUserProperties.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveUserProperties.setVisibility(View.VISIBLE);
                userName.setEnabled(true);
                editUserProperties.setVisibility(View.INVISIBLE);
                userName.requestFocus();
                imm.showSoftInput(userName, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        saveUserProperties.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editUserProperties.setVisibility(View.VISIBLE);
                userName.setEnabled(false);
                saveUserProperties.setVisibility(View.INVISIBLE);
                sharedPreferences.edit().putString("userName", String.valueOf(userName.getText())).apply();
            }
        });

        /*Before user starts to edit call editDialog in order to know
        * if user is going to create a new group or changing to existing one*/
        editGroupProperties.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editDialog();
            }
        });

        /*After long iteration and checks if user is happy with the result
        * submitGroupChanges. Either update shared preferences with the ne
        * groups properties or create a new one to the database*/
        submitGroupChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                groupEditStatus.setText(R.string.group_name);
                GradientDrawable drawable = (GradientDrawable) groupName.getBackground();
                drawable.setStroke(3, Color.rgb(0, 0, 0));

                if (changingOrCreating.equals("change")) {
                    groupName.setEnabled(false);
                    updateSharedPreferences(updatableJSONObject[0]);
                } else if (changingOrCreating.equals("create")) {
                    groupName.setEnabled(false);
                    routeSpinner.setEnabled(false);
                    String groupToCreate = groupName.getText().toString();
                    String routeToPick = routeSpinner.getSelectedItem().toString();
                    createGroup(groupToCreate, routeToPick);
                }

            }
        });

        updateGroupErrorTextView();

        return rootView;
    }


    /*checkIfGroupExists in the database with the given groupName (=groupToCompare).
    * Also compare the results if user was changingOrCreating group*/
    public void checkIfGroupExists(final String groupToCompare, final String changingOrCreating) {
        groupNameExists = false;

        Log.e("checkIfGroupExists  ", groupToCompare + " " + changingOrCreating);

        final JSONObject[] matchJSONObject = new JSONObject[1];
        /*Make a simple GET request to the database and add a callback function
        to it so that when the AsyncTack is completed this fragment knows about it.*/
        SimpleRequest getMeGroups = new SimpleRequest(new FragmentCallback() {

            @Override
            public void onTaskDone(String result) {

                Log.e("result: ", result);

                if (result.contains("No route to host")) {
                    showToast("Couldn't connect to server.");
                } else {

                /*Create a JSONArray based on the result we get from the dataBase and iterate it*/
                    try {
                        JSONArray mainJSONObject = new JSONArray(result.toString());

                        for (int i = 0; i < mainJSONObject.length(); i++) {

                            JSONObject tempJSONObject = (JSONObject) mainJSONObject.get(i);
                            String currentGroupName = tempJSONObject.getString("groupName");

                    /*If groupToCompare exists in the database set groupNameExists to true
                    * so that after loop we can make "assumptions based on it"*/
                            if (currentGroupName.equals(groupToCompare)) {
                                matchJSONObject[0] = tempJSONObject;
                                groupNameExists = true;
                            }
                        }

                  /*Check whether or not the groupName existed in the  JSONArray
                  * If the user was about create a group, finding one in the database is not good
                  * But if the user was changing group finding one in database is a good thing.
                  * Two successes and two fails*/
                        if (groupNameExists) {
                            if (changingOrCreating.equals("change")) {
                                //SUCCESS => CHANGE GROUP
                                //groupName.setBackgroundColor(getResources().getColor(R.color.highLightSuccess));
                                GradientDrawable drawable = (GradientDrawable) groupName.getBackground();
                                drawable.setStroke(3, Color.rgb(104, 225, 114));
                                submitGroupChanges.setEnabled(true);
                                submitGroupChanges.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.buttonPrimaryColor));

                                updatableJSONObject = matchJSONObject;
                            } else if (changingOrCreating.equals("create")) {
                                //FAIL INFORM USER GROUP NAME IS TAKEN
                                //groupName.setBackgroundColor(getResources().getColor(R.color.highLightFailure));
                                GradientDrawable drawable = (GradientDrawable) groupName.getBackground();
                                drawable.setStroke(3, Color.rgb(218, 67, 72));
                                submitGroupChanges.setEnabled(false);
                                submitGroupChanges.setBackgroundColor(Color.LTGRAY);

                            }
                        } else if (!groupNameExists) {
                            if (changingOrCreating.equals("create")) {
                                //SUCCESS => CREATE GROUP
                                //groupName.setBackgroundColor(getResources().getColor(R.color.highLightSuccess));
                                GradientDrawable drawable = (GradientDrawable) groupName.getBackground();
                                drawable.setStroke(3, Color.rgb(104, 225, 114));
                                submitGroupChanges.setEnabled(true);
                                submitGroupChanges.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.buttonPrimaryColor));


                            } else if (changingOrCreating.equals("change")) {
                                //FAIL INFORM USER THAT NAME DOENS'T EXIST IN THE DATABSE
                                // groupName.setBackgroundColor(getResources().getColor(R.color.highLightFailure));
                                GradientDrawable drawable = (GradientDrawable) groupName.getBackground();
                                drawable.setStroke(3, Color.rgb(218, 67, 72));
                                submitGroupChanges.setEnabled(false);
                                submitGroupChanges.setBackgroundColor(Color.LTGRAY);

                            }
                        }
                    } catch (Exception e) {
                    }
                }

            }
        });
        /*Execute getMeGroups method*/
        getMeGroups.execute(sharedPreferences.getString("ip", "no ip") + "groups/");
    }

    /*This gets called only if onGetMeGroupTaskIsDone
     and  groupToCreate doesn't exist in the database*/
    public void createGroup(String groupToCreate, String pickedRoute) {

        JSONObject requestParameters = new JSONObject();

        try {
            requestParameters.put("groupName", groupToCreate);
            requestParameters.put("groupRoute", pickedRoute);
            requestParameters.put("groupSteps", 0);
        } catch (Exception e) {
        }

        Log.e("createGroup params", String.valueOf(requestParameters));

        SendRequest createGroupTask = new SendRequest(new FragmentCallback() {

            @Override
            public void onTaskDone(String result) {
                Log.e("onTaskDone ", result);
                if (result.equals("Failed")) {
                    showToast("Couldn't create a group.");
                } else {
                    try {
                        JSONObject resultJSONObject = new JSONObject(result.toString());
                        updateSharedPreferences(resultJSONObject);
                    } catch (Exception e) {

                    }
                }


            }
        }, sharedPreferences.getString("ip", "no ip") + "groups/createGroup", "POST", requestParameters) {

            @Override
            public void onTaskDone(String result) {
            }

        };

        createGroupTask.execute();
    }

    public void updateSharedPreferences(JSONObject resultJSONObject) {
        Log.e("usp result: ", String.valueOf(resultJSONObject));
        updateGroupErrorTextView();

        try {
            sharedPreferences.edit().putString("groupName", resultJSONObject.getString("groupName")).commit();
            sharedPreferences.edit().putString("groupRoute", resultJSONObject.getString("groupRoute")).commit();
            if (changingOrCreating.equals("change")) {
                sharedPreferences.edit().putString("groupId", resultJSONObject.getString("groupId")).commit();
                showToast("Group changed!");
            } else if (changingOrCreating.equals("create")) {
                sharedPreferences.edit().putString("groupId", resultJSONObject.getString("_id")).commit();
                showToast("Group created!");
            }


            ArrayAdapter routeSpinnerAdapter = (ArrayAdapter) routeSpinner.getAdapter(); //cast to an ArrayAdapter
            int spinnerPosition = routeSpinnerAdapter.getPosition(sharedPreferences.getString("groupRoute", "noRoute"));
            routeSpinner.setSelection(spinnerPosition);

        } catch (Exception e) {
        }

        submitGroupChanges.setEnabled(false);
        submitGroupChanges.setBackgroundColor(Color.LTGRAY);

    }

    public interface FragmentCallback {
        void onTaskDone(String result);

    }

    /**
     * Notification methods
     */

    /*Generic method to show toast for short period of time*/
    public void showToast(String toastText) {
        Toast toast = Toast.makeText(getActivity(), toastText, Toast.LENGTH_SHORT);
        toast.show();
    }

    /*welcomeDialog that opens when user launches application for the first time*/
    public void welcomeDialog() {
        final EditText input = new EditText(getActivity());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setHint("Username");
        input.setLayoutParams(lp);

        new AlertDialog.Builder(getActivity())

                .setTitle("Welcome to J.A.H.A.S!")
                .setMessage("Just Another Health Application Service \n" +
                        "Start your journey by registering to the system. \n" +
                        "Then create a group or join existing one!")

                .setCancelable(false)
                .setView(input)
                .setPositiveButton("Register", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        sharedPreferences.edit().putString("userName", String.valueOf(input.getText())).apply();
                        userName.setText(sharedPreferences.getString("userName", ""));
                    }
                })
                .show();
    }


    /*editDialog determines whether the user is going to create a new group or join new one*/
    public void editDialog() {

        // Strings to Show In Dialog with Radio Buttons
        final CharSequence[] items = {"Create group ", " Change group "};

        // Creating and Building the Dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Do you want to create or change group?");
        builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                switch (item) {
                    case 0:
                        groupName.setEnabled(true);
                        routeSpinner.setEnabled(true);
                        changingOrCreating = "create";
                        groupEditStatus.setText("Create group: ");
                        // groupName.setBackgroundColor(0);
                        break;
                    case 1:
                        groupName.setEnabled(true);
                        routeSpinner.setEnabled(false);
                        changingOrCreating = "change";
                        groupEditStatus.setText("Change to group: ");
                        //groupName.setBackgroundColor(0);
                        break;

                }
                groupName.setText("");
                groupName.setFocusableInTouchMode(true);
                levelDialog.dismiss();
                groupName.requestFocus();
                imm.showSoftInput(groupName, InputMethodManager.SHOW_IMPLICIT);
            }
        });
        levelDialog = builder.create();
        levelDialog.show();
    }

    public void updateGroupErrorTextView() {
        //If no group info, show error
        if (sharedPreferences.getString("groupId", "").equals("")) {
            groupErrorTextView.setVisibility(View.VISIBLE);
        } else {
            groupErrorTextView.setVisibility(GONE);
        }
    }

}



