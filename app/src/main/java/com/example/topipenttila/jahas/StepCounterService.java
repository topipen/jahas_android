package com.example.topipenttila.jahas;

import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import static android.R.attr.defaultValue;
import static com.google.android.gms.analytics.internal.zzy.s;


/**
 * Created by topipenttila on 04/10/16.
 */

public class StepCounterService extends Service implements SensorEventListener {

    private SensorManager sensorManager;
    private boolean firstTime;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private PowerManager mgr;
    private PowerManager.WakeLock wakeLock;
    private Sensor countSensor;

    public StepCounterService(Context applicationContext) {
        super();

    }

    public StepCounterService() {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        firstTime = true;
        mgr = (PowerManager)getSystemService(Context.POWER_SERVICE);
        wakeLock = mgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakeLock");
        // Acquire wakelock to enable background step counting
        if (!wakeLock.isHeld()) wakeLock.acquire();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = sharedPreferences.edit();
        long stepsSinceLastCommit = sharedPreferences.getInt("stepsSinceLastCommit", 0);
        editor.putInt("savedStepValue", (int)stepsSinceLastCommit);
        editor.putBoolean("firstTimeSensorChanged", true);
        editor.apply();
        Log.i("HERE", "here I am!");
        sensorManager = (SensorManager)getApplicationContext().getSystemService(Context.SENSOR_SERVICE);
        countSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        if (countSensor != null) {
            sensorManager.registerListener(this, countSensor, SensorManager.SENSOR_DELAY_UI);
        } else {
            Toast.makeText(this, "Step counter sensor not available", Toast.LENGTH_LONG).show();
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("EXIT", "ondestroy!");
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("Broadcast");
        sendBroadcast(broadcastIntent);
        if (wakeLock.isHeld()) wakeLock.release();

    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        //If first time changing, reset the starting sensor value
        firstTime = sharedPreferences.getBoolean("firstTimeSensorChanged", false);
        if (firstTime) {
            editor.putInt("startingStepValue", (int)event.values[0]);
            editor.putBoolean("firstTimeSensorChanged", false);
            editor.apply();

        }
        // stepsSinceLastCommit = valueFromSensor - startingStepValue + savedStepValue (value from previous sensor services)
        int stepsSinceLastCommit = (int)event.values[0] - sharedPreferences.getInt("startingStepValue", 0) + sharedPreferences.getInt("savedStepValue", 0);
        editor.putInt("stepsSinceLastCommit", stepsSinceLastCommit);
        editor.apply();
        EventBus.getDefault().post(new MessageEvent("Hello everyone!"));

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

}
