package com.example.topipenttila.jahas;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.geojson.GeoJsonFeature;
import com.google.maps.android.geojson.GeoJsonLayer;
import com.google.maps.android.geojson.GeoJsonLineStringStyle;
import com.google.maps.android.geojson.GeoJsonPointStyle;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;


import java.io.IOException;

/**
 * Created by iosdev on 28.9.2016.
 */

public class MapViewFragment extends Fragment {

    SharedPreferences sharedPreferences;

    MapView mMapView;
    private GoogleMap googleMap;
    private int groupSteps;
    private String groupName;
    private double groupDistance;
    private int comparableStep;
    private double latitude;
    private double longitude;
    private LatLng startPosition;
    private String cityName;
    private String citySteps;
    GeoJsonLayer layer = null;


    TextView groupStepsTextView ,groupDistanceTextView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.location_fragment, container, false);
        //create reference for sharedPreferences and get default preferences
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        groupStepsTextView = (TextView) rootView.findViewById(R.id.groupSteps);
        groupDistanceTextView = (TextView) rootView.findViewById(R.id.groupDistance);


        /*AsyncTask to load the google map*/
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;
                mMap.getUiSettings().setMapToolbarEnabled(false);

                /*MarkerClickListener listens to  when markers are being clicked
                * and handles the event forward if group has enough steps*/
                mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {

                        if (groupSteps != -1) {
                            comparableStep = Integer.parseInt(marker.getSnippet());

                            if (groupSteps >= comparableStep) {
                                System.out.println("marker: " + marker.getId() + "markerTitle: " + marker.getTitle() + "markerLocation: " + marker.getPosition());
                                checkIfCityExists(marker.getTitle());
                            }

                        }else {
                            showToast("No group's steps available.");
                        }
                        return false;
                    }
                });

                //

                try {
                    /*Get a reference id tot the groups picked route using getResources().getIdentifier
                    * And load that GeoJsonn route to the map*/
                    int resID = getResources().getIdentifier(sharedPreferences.getString("groupRoute", ""), "raw", getActivity().getPackageName());
                    Log.e("resID: ", String.valueOf(resID));
                    layer = new GeoJsonLayer(mMap, resID, getActivity().getApplicationContext());

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                layer.addLayerToMap();

                 /*When launching this fragment getGroupSteps in order to know what parts of the route
                 * should be visible and clickable. Also show the number of steps in text view*/
                getGroupSteps();

            }
        });
        return rootView;
    }


    public void updateMap(){
       /*Loop through the GeoJson features and modify the style of the route accordingly*/
        for (GeoJsonFeature feature : layer.getFeatures()) {

            if(feature.hasProperty("startLat")){

                latitude = Double.parseDouble(feature.getProperty("startLat"));
                longitude = Double.parseDouble(feature.getProperty("startLng"));

                startPosition = new LatLng(latitude,longitude );

                // For zooming automatically to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(startPosition).zoom(9).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }

            /*If there's a property called title we're currently at marker feature
            * get cityName and citySteps from the marker and set them to be
            * the currents pointStyle's properties and add that style to the marker.*/
            if (feature.getGeometry().getType().equals("Point")){

                GeoJsonPointStyle pointStyle = new GeoJsonPointStyle();

                //Set correct style for te markers = cityName to marker marker title and citySteps to snippet
                cityName = feature.getProperty("title");
                citySteps = feature.getProperty("steps");
                pointStyle.setTitle(cityName);
                pointStyle.setSnippet(citySteps);
                feature.setPointStyle(pointStyle);

                //If groups steps equal to the value to unlock the next city, change the color of the marker
                if(Integer.parseInt(feature.getProperty("steps"))<= groupSteps){
                    pointStyle.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));

                }else if(Integer.parseInt(feature.getProperty("steps")) > groupSteps){
                    pointStyle.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                }
            }

            if (feature.getGeometry().getType().equals("LineString")){

                GeoJsonLineStringStyle lineStringStyle = new GeoJsonLineStringStyle();

                //If groups steps equal to the value to unlock the next city, change the color of the linestring
                if(Integer.parseInt(feature.getProperty("steps")) <= groupSteps){
                    lineStringStyle.setColor(Color.rgb(104,225,114));
                    feature.setLineStringStyle(lineStringStyle);
                }else {
                    lineStringStyle.setColor(Color.rgb(218,67,72));
                    feature.setLineStringStyle(lineStringStyle);
                }
            }
        }
    }


    /*Check if cityToCompare exists in the database and get all the relevant data of
    * that city and openCityPopUp() with those parameters. Much similar as the
    * checkIfGroupExists() in AccountFragment*/
    public void checkIfCityExists(final String cityToCompare){

        SimpleRequest getCityTask = new SimpleRequest(new FragmentCallback() {
            final JSONObject[] matchJSONObject = new JSONObject[1];

            @Override
            public void onTaskDone(String result) {

                if(result.contains("No route to host")){
                    showToast("Couldn't get city data");
                }else {
                    try {
                        JSONArray mainJSONObject = new JSONArray(result.toString());

                        for(int i=0; i<mainJSONObject.length(); i++) {

                            JSONObject tempJSONObject = (JSONObject) mainJSONObject.get(i);
                            String currentCityName = tempJSONObject.getString("cityName");

                            if (currentCityName.equals(cityToCompare)) {
                                matchJSONObject[0] = tempJSONObject;
                                openCityPopUp(matchJSONObject[0]);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        getCityTask.execute(sharedPreferences.getString("ip" , "no ip") + "cities/");
    }

    /*openCityPopUp with the parameters of the clicked city marker.
    * PopUp can be closed from button or just by clicking the view itself.*/
    public void openCityPopUp(JSONObject cityJSONObject){

        final PopupWindow descriptionPopup;
        View popUpView = getActivity().getLayoutInflater().inflate(R.layout.description_popup, null); // inflating popup layout

        descriptionPopup = new PopupWindow(popUpView, RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        descriptionPopup.setAnimationStyle(android.R.style.Animation_Dialog);
        descriptionPopup.setElevation(10);
        descriptionPopup.showAtLocation(popUpView, Gravity.CENTER, 0, 0);// displaying popup

        /*descriptionPopup widgets and their references*/
        final Button dismiss = (Button) popUpView.findViewById(R.id.dismiss);
        final TextView cityName = (TextView) popUpView.findViewById(R.id.cityName);
        final TextView cityCapita = (TextView) popUpView.findViewById(R.id.cityCapita);
        final TextView cityDescription = (TextView) popUpView.findViewById(R.id.cityDescription);
        final TextView cityTrivia = (TextView) popUpView.findViewById(R.id.cityTrivia);

        /*Set correct data to the descriptionPopup*/
        try {
            cityName.setText(cityJSONObject.getString("cityName"));
            cityCapita.setText(String.valueOf(cityJSONObject.getInt("cityCapita")));
            cityDescription.setText((cityJSONObject.getString("cityDescription")));
            cityTrivia.setText(cityJSONObject.getString("cityTrivia"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                descriptionPopup.dismiss();
            }
        });

        popUpView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                descriptionPopup.dismiss();
            }
        });
    }

    /*getGroupSteps in order to compare to the markers required step properties and to show them to the user.*/
    public void getGroupSteps(){

        /*groupSteps are received by the groupId which is saved once user creates a new group
        *or signs into an existing group*/
        SimpleRequest getGroupStepsTask = new SimpleRequest(new FragmentCallback() {

            @Override
            public void onTaskDone(String result) {
                Log.e("getGroupSteps result: " , result);

                if(result.contains("No route to host")){
                    showToast("Couldn't get group's steps");
                    groupSteps = -1;
                }else {
                    try{
                        JSONObject resultJSON = new JSONObject(result);
                        groupSteps = resultJSON.getInt("groupSteps");
                        groupName = resultJSON.getString("groupName");
                        groupStepsTextView.append(" " + groupName + " " + String.valueOf(groupSteps));
                        groupDistance = Math.round((groupSteps * 0.7)/1000 * 10) / 10.0;
                        groupDistanceTextView.append(String.valueOf(groupDistance) + "km");
                        updateMap();
                    }catch(JSONException e){
                    }
                }
            }
        });

        getGroupStepsTask.execute(sharedPreferences.getString("ip" , "no ip")+"groups/"+sharedPreferences.getString("groupId", "no Id"));
    }





    public interface FragmentCallback {
        void onTaskDone(String result);

    }

    /*Generic method to show toast for short period of time*/
    public void showToast(String toastText){
        Toast toast = Toast.makeText(getActivity(), toastText,Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

}