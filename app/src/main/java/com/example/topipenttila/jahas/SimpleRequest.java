package com.example.topipenttila.jahas;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.text.format.Formatter;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Juhani on 6.10.2016.
 *  class communicates with the API
 */



public class SimpleRequest extends AsyncTask<String, Void, String> {

    private AccountFragment.FragmentCallback mFragmentCallback;
    private MapViewFragment.FragmentCallback mapFragmentCallback;

    public SimpleRequest(AccountFragment.FragmentCallback fragmentCallback) {
        mFragmentCallback = fragmentCallback;
    }

    public SimpleRequest(MapViewFragment.FragmentCallback fragmentCallback){
        mapFragmentCallback = fragmentCallback;
    }

    

    @Override
    protected String doInBackground(String... strings) {

        String result;
        try {
            URL url = new URL(strings[0]);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            InputStream stream = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
            StringBuilder builder = new StringBuilder();

            String inputString;

            while ((inputString = bufferedReader.readLine()) != null) {
                builder.append(inputString);
            }

            System.out.println("BUILDER: "+builder);

            //mainJSONOBJECT
            //JSONObject mainJSONObject = new JSONObject(builder.toString());
            //result = String.valueOf(mainJSONObject);
            result = builder.toString();
            //groupName = String.valueOf(mainJSONObject.getString("groupName"));

            urlConnection.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
            result = e.getMessage();
        }

        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        System.out.println("Simple request result: "+result);
        if(mapFragmentCallback != null){
            mapFragmentCallback.onTaskDone(result);

        }else if(mFragmentCallback != null){
            mFragmentCallback.onTaskDone(result);

        }


    }
}




